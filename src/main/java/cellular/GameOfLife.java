package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;


public class GameOfLife implements CellAutomaton {



	IGrid currentGeneration;


	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		// TODO
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		// TODO
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		// TODO
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// TODO
		for(int row=0; row<numberOfRows();row++) {
			for(int col=0 ; col <numberOfColumns(); col++) {
				CellState state = getNextCell(row,col);
				nextGeneration.set(row, col, state);
			}
		}

		currentGeneration = nextGeneration;

	}

	@Override
	public CellState getNextCell(int row, int col) {
		int count = this.countNeighbors(row, col, CellState.ALIVE);
		CellState currentState = currentGeneration.get(row, col);


		if (currentState == CellState.ALIVE) {
			if (count < 2) {
				return CellState.DEAD;
			} else if (count == 2 || count == 3) {
				return CellState.ALIVE;
			} else if (count > 3) {
				return CellState.DEAD;
			}
		} else {
			if (count == 3) {
				return CellState.ALIVE;
			}
		}
		return currentState;
	}



	private int countNeighbors(int row, int col, CellState state) {
		// TODO
		int count = 0;
		for(int i = row - 1; i <= row + 1; i++) {
			for(int j = col - 1; j <= col + 1; j++) {
				try {
					if (getCellState(i, j).equals(state)) {
						if (i != row || j != col) {
							count++;
						}
					}
				}
				catch (IndexOutOfBoundsException e) {
					;
				}
			}
		}
		return count;
	}


	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
