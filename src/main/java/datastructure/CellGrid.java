package datastructure;

import cellular.CellState;


public class CellGrid implements IGrid {
    int cols;
    int rows;
    CellState[][] grid;



    public CellGrid(int rows, int columns, CellState initialState) {
        // TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                this.grid[i][j] = initialState;
            }
        }



    }

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element){
        // TODO Auto-generated method stub
        if(row >= this.numRows() || column >= this.numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        if(row < 0 || column < 0) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;
    }




    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if(row >= this.numRows() || column >= this.numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        if(row < 0 || column < 0) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];

    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for(int row=0; row<this.rows; row++) {
            for(int col=0; col<this.cols; col++) {
                newGrid.set(row, col, this.get(row, col));
            }
        }
        return newGrid;

    }

}
